package fr.nuroz.chunk_base_live.commandes;

import fr.nuroz.chunk_base_live.Main;
import fr.nuroz.chunk_base_live.MyPlayer;
import fr.nuroz.chunk_base_live.MyWebSocketServer;
import fr.nuroz.chunk_base_live.PlayerInteractor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.java_websocket.WebSocket;

public class AccepterOuRefuser implements CommandExecutor {
    private final boolean accepter;

    public AccepterOuRefuser(boolean accepter) {
        this.accepter = accepter;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        // si la commande est exécuté par un joueur
        if(sender instanceof Player) {

            // alors on récupère le joueur
            MyPlayer p = PlayerInteractor.getPlayer(sender.getName());

            // si le joueur est bien présent
            if(p == null) {
                return false;
            }

            // alors on vérifie s'il a une demande en cours ou pas
            if(!p.hasRequest()) {
                p.getPlayer().sendMessage("Vous n'avez pas de demande en cours");
                return false;
            }

            // si le joueur a une demande en cours
            // alors on recupere le client et lui definie la valeur saisie par le joueur
            WebSocket requestUser = p.getRequest();
            MyWebSocketServer.clients.get(requestUser).setSecond(accepter);

            // on envoie la réponse au client
            requestUser.send("{\"message\": \"Le joueur vous a " + (accepter ?  "accepter" : "refuser") + "\", \"success\": " + accepter + "}");

            // si le joueur a accepter la demande
            p.getPlayer().sendMessage("Vous avez accepter la demande");
            p.removeRequest(p.getRequest());
        }

        return false;
    }
}
