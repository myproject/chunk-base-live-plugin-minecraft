package fr.nuroz.chunk_base_live.utils;

public class Pair<T, X> {
    private T first;
    private X second;

    public Pair(T key, X value) {
        this.first = key;
        this.second = value;
    }

    public T getFirst() {
        return first;
    }

    public X getSecond() {
        return second;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public void setSecond(X second) {
        this.second = second;
    }
}
