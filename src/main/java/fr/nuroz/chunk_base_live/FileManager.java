package fr.nuroz.chunk_base_live;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileManager {

    /**
     * Permet de vérifier si un fichier existe
     * @param file Le fichier à vérifier
     * @return true si le fichier existe, false sinon
     */
    public boolean isFileExist(File file) {
        return file.exists() && file.isFile();
    }

    public String readFile(File file) {
        String result = null;
        try {
            result = Files.readString(Paths.get(file.getAbsolutePath()));
        } catch (IOException e) {
            System.out.println("ChunkBaseLive: Impossible de lire le fichier: " + file.getName());
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Permet de crée un fichier
     * @param file Le fichier à crée
     * @param content Le contenu du fichier
     */
    public void createFileWithContent(File file, String content) {
        try {
            if(file.createNewFile()) {
                FileWriter fw = new FileWriter(file);
                fw.write(content);
                fw.close();

                System.out.println("ChunkBaseLive: Fichier " + file.getName() + " créee et remplie avec succès");
            } else {
                System.out.println("ChunkBaseLive: Impossible de crée le fichier: " + file.getName());
            }
        } catch (IOException e) {
            System.out.println("ChunkBaseLive: Impossible de crée le fichier: " + file.getName());
            e.printStackTrace();
        }
    }
}
