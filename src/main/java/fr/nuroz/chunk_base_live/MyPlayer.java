package fr.nuroz.chunk_base_live;

import org.bukkit.entity.Player;
import org.java_websocket.WebSocket;

import java.util.ArrayList;
import java.util.List;

public class MyPlayer {
    private final Player player;
    private final List<WebSocket> request = new ArrayList<>();

    public MyPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public void addRequest(WebSocket webSocket) {
        request.add(webSocket);
    }

    public void removeRequest(WebSocket webSocket) {
        request.remove(webSocket);
    }

    public boolean hasRequest() {
        return request.size() > 0;
    }

    public WebSocket getRequest() {
        return request.get(0);
    }
}
