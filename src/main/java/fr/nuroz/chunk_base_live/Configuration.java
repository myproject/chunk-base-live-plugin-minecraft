package fr.nuroz.chunk_base_live;

import org.json.JSONObject;

public class Configuration {
    private int ListenPort = 8080;

    /**
     * Permet d'initialiser la configuration à partir d'un objet JSON
     * @param config Objet JSON contenant la configuration
     */
    public void init(JSONObject config) {
        if(config.has("ListenPort") && config.get("ListenPort") instanceof Integer) {
            ListenPort = config.getInt("ListenPort");
        }
    }

    /**
     * Permet de récupérer le port d'écoute du serveur
     * @return Port d'écoute du serveur
     */
    public int getListenPort() {
        return ListenPort;
    }

    /**
     * Permet de récupérer la configuration par défaut au format JSON
     * @return Configuration par défaut au format JSON
     */
    public String getDefautValueToJson() {
        return "{\"ListenPort\": 8080}";
    }
}
