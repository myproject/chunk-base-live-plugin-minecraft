package fr.nuroz.chunk_base_live;

import fr.nuroz.chunk_base_live.commandes.AccepterOuRefuser;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class Main extends JavaPlugin implements Listener
{
    public static File SAVEFILE;
    public static Configuration configuration;
    public static List<MyPlayer> onlinePlayers = new ArrayList<>();

    @Override
    public void onEnable()
    {
        // init configuration
        SAVEFILE = new File("plugins/chunkBaseLiveConfig.json");
        configuration = new Configuration();
        FileManager fileManager = new FileManager();

        // si le fichier chunkBaseLiveConfig.save exist
        if(fileManager.isFileExist(SAVEFILE)) {

            // alors on le lit et initialise la configuration avec
            String file = fileManager.readFile(SAVEFILE);

            if(file != null) {
                configuration.init(new JSONObject(file));
            }
        } else {

            // sinon on le crée avec la valeur par défaut
            fileManager.createFileWithContent(SAVEFILE, configuration.getDefautValueToJson());
        }

        // set event manager on plugin
        getServer().getPluginManager().registerEvents(this, this);

        // start websocket server
        MyWebSocketServer.startServeur(configuration.getListenPort());

        // set commande accepter and refuser
        getCommand("accepter").setExecutor(new AccepterOuRefuser(true));
        getCommand("refuser").setExecutor(new AccepterOuRefuser(false));

        // loop task for send position
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, MyWebSocketServer::sendPosition, 0, 20L);

        System.out.println("ChunkBaseLive: le plugin vien de s'allumer");
    }

    @Override
    public void onDisable() {
        // stop websocket server
        MyWebSocketServer.closeServeur();
        System.out.println("ChunkBaseLive: le plugin vien de s'éteindre");
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        // add player to onlinePlayers
        onlinePlayers.add(new MyPlayer(event.getPlayer()));
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {

        // remove player from onlinePlayers
        MyPlayer p = PlayerInteractor.getPlayer(event.getPlayer().getName());

        MyWebSocketServer.closeConnectionWithClient(p);

        onlinePlayers.remove(p);
    }
}

