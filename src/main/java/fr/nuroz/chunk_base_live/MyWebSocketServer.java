package fr.nuroz.chunk_base_live;

import fr.nuroz.chunk_base_live.utils.Pair;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Optional;

public class MyWebSocketServer extends WebSocketServer {

    private static MyWebSocketServer serveur;
    public static HashMap<WebSocket, Pair<Optional<String>, Boolean>> clients = new HashMap<>();

    /**
     * Permet de démarer le serveur WebSocket
     * @param port le port sur lequel le serveur doit écouter
     */
    public static void startServeur(int port) {
        serveur = new MyWebSocketServer(port);
        serveur.start();
        System.out.println("ChunkBaseLive: Serveur WebSocket démarré sur le port " + port);
    }

    /**
     * Permet d'arrêter le serveur WebSocket
     */
    public static void closeServeur() {
        try {
            serveur.stop();
            System.out.println("ChunkBaseLive: Serveur WebSocket arrêté");
        } catch (IOException | InterruptedException e) {
            System.out.println("ChunkBaseLive: Impossible d'arrêter le serveur WebSocket");
            e.printStackTrace();
        }
    }

    /**
     * Permet d'envoyer la position des joueurs au client WebSocket
     */
    public static void sendPosition() {
        // pour chaque client
        clients.forEach((webSocket, client) -> {

            // si la demande a été acceptée et que le nom de l'utilisateur est bien présent
            if(client.getSecond() && client.getFirst().isPresent()) {

                // alors on récupère le joueur
                MyPlayer p = PlayerInteractor.getPlayer(client.getFirst().get());

                // et on envoie sa position au client
                if(p != null) {
                    webSocket.send(String.format("{\"position\": {\"x\": %s, \"y\": %s, \"z\": %s}}",
                            p.getPlayer().getLocation().getX(), p.getPlayer().getLocation().getY(), p.getPlayer().getLocation().getZ()));
                }
            }
        });
    }

    public MyWebSocketServer(int port) {
        super(new InetSocketAddress(port));
    }

    @Override
    public void onMessage(WebSocket webSocket, String s) {
        // si on recoit le message "stop" alors on ferme la connexion
        if(s.equals("stop")) {
            webSocket.close();
            return;
        }

        // on récupère le client
        Pair<Optional<String>, Boolean> client = clients.get(webSocket);

        // on parse le message en JSON
        JSONObject json = new JSONObject(s);

        // si le message ne contient pas de username
        if(!json.has("username")) {
            webSocket.send("{\"message\": \"Votre message n'est pas au format JSON\", \"erreur\": true}");
            return;
        }

        String username = json.getString("username");

        // si le username n'est pas présent dans la liste des joueurs connectés
        if(!PlayerInteractor.isPlayerExist(username)) {
            webSocket.send("{\"message\": \"Le joueur " + username + " n'est pas connecté\", \"erreur\": true}");
            return;
        }

        // si le client a déjà envoyé une demande à ce joueur
        if(client.getFirst().isPresent() && client.getFirst().get().equals(username)) {
            webSocket.send("{\"message\": \"Vous avez déjà envoyé une demande à " + username + "\", \"erreur\": true}");
            return;
        }

        // on envoie la demande au joueur et on met à jour le client
        PlayerInteractor.sendRequestToPlayer(webSocket, username);
        client.setFirst(Optional.of(username));
        client.setSecond(false);
        webSocket.send("{\"message\": \"Demande envoyée\", \"erreur\": false}");
    }

    /**
     * permet de fermer la connexion avec le client qui observe le joueur p
     * @param p le joueur
     */
    public static void closeConnectionWithClient(MyPlayer p) {
        clients.forEach((webSocket, client) -> {
            if(client.getFirst().isPresent() && client.getFirst().get().equals(p.getPlayer().getName())) {
                webSocket.close();
            }
        });
    }

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        clients.put(webSocket, new Pair<>(Optional.empty(), false));
    }

    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {clients.remove(webSocket);}

    @Override
    public void onError(WebSocket webSocket, Exception e) {clients.remove(webSocket);}

    @Override
    public void onStart() {}
}
