package fr.nuroz.chunk_base_live;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.awt.*;
import net.md_5.bungee.api.chat.TextComponent;
import org.java_websocket.WebSocket;

public class PlayerInteractor {
    /**
     * Permet de vérifier si un joueur est connecté
     * @param username Nom du joueur
     * @return boolean
     */
    public static boolean isPlayerExist(String username) {
        return Main.onlinePlayers.stream()
            .anyMatch(player ->
                player.getPlayer().getName().equals(username)
            );
    }

    /**
     * Permet de récupérer un joueur
     * @param username Nom du joueur
     * @return Player
     */
    public static MyPlayer getPlayer(String username) {
        return Main.onlinePlayers.stream()
            .filter(player ->
                player.getPlayer().getName().equals(username)
            ).findFirst()
            .orElse(null);
    }

    /**
     * Permet d'envoyer une demande de partage de position à un joueur
     * @param ws WebSocket
     * @param username Nom du joueur
     */
    public static void sendRequestToPlayer(WebSocket ws, String username) {
        // récupère le joueur
        MyPlayer player = getPlayer(username);

        // si le joueur n'est pas connecté
        if(player == null) {
            return;
        }

        // on ajoute la demande au MyPlayer
        player.addRequest(ws);

        // Créez un composant de texte
        TextComponent message = new TextComponent("Acceptez vous de partager votre position en direct ?\n");

        // Créez le texte cliquable "Accepter"
        TextComponent accepter = new TextComponent("[Accepter]");
        accepter.setColor(ChatColor.GREEN);
        accepter.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/accepter"));

        // Créez le texte cliquable "Refuser"
        TextComponent refuser = new TextComponent("[Refuser]");
        refuser.setColor(ChatColor.RED);
        refuser.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/refuser"));

        // Ajoutez les options "Accepter" et "Refuser" au message
        message.addExtra(accepter);
        message.addExtra(" ou ");
        message.addExtra(refuser);

        // Envoyez le message au joueur
        player.getPlayer().spigot().sendMessage(message);
    }
}
